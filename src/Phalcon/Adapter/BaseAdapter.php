<?php
namespace Mvs\Logger\Phalcon\Adapter;

use Phalcon\Di\DiInterface;
use Phalcon\Di\InjectionAwareInterface;
use Phalcon\Logger;
use Phalcon\Logger\Adapter\AbstractAdapter;
use Phalcon\Logger\Adapter\AdapterInterface;
use Phalcon\Logger\Item;

abstract class BaseAdapter extends AbstractAdapter implements InjectionAwareInterface
{
    public const CONFIG_MIN_LOG_LEVEL = 'min_log_level';
    protected const DEFAULT_MIN_LOG_LEVEL = Logger::DEBUG;

    /**
     * @var array
     */
    protected $options = [];

    /**
     * @var DiInterface
     */
    protected $di;

    abstract public function getName(): string;

    public function __construct(array $options=[])
    {
        $this->setOptions($options);
    }

    public function add(Item $item): AdapterInterface
    {
        if ($this->shouldLog($item)) {
            parent::add($item);
        }

        return $this;
    }

    public function close(): bool
    {
        return true;
    }

    /**
     * @param string $name
     * @return mixed|null
     */
    public function getOption(string $name)
    {
        return $this->options[$name] ?? null;
    }

    public function logLevelToString(int $level): string
    {
        switch ($level) {
            case Logger::EMERGENCY:
                return 'emergency';
            case Logger::CRITICAL:
                return 'critical';
            case Logger::ALERT:
                return 'alert';
            case Logger::ERROR:
                return 'error';
            case Logger::WARNING:
                return 'warning';
            case Logger::NOTICE:
                return 'notice';
            case Logger::INFO:
                return 'info';
            case Logger::DEBUG:
            default:
                return 'debug';
        }
    }

    protected function setOptions(array $options=[]): void
    {
        if (!isset($options[self::CONFIG_MIN_LOG_LEVEL])) {
            $options[self::CONFIG_MIN_LOG_LEVEL] = self::DEFAULT_MIN_LOG_LEVEL;
        }

        $this->options[self::CONFIG_MIN_LOG_LEVEL] = $options[self::CONFIG_MIN_LOG_LEVEL];
    }

    protected function shouldLog(Item $item): bool
    {
        return ($item->getType() <= $this->getOption(self::CONFIG_MIN_LOG_LEVEL));
    }

    protected function logLoggerFailure(string $message): void
    {
        /**
         * @var Logger $logger
         */
        $logger = $this->di
            ->get('logger');

        $logger
            ->excludeAdapters([$this->getName()])
            ->critical($message);
    }

    public function setDI(DiInterface $container): void
    {
        $this->di = $container;
    }

    public function getDI(): DiInterface
    {
        return $this->di;
    }
}