<?php
namespace Mvs\Logger\Phalcon\Adapter;

use Phalcon\Db\Column;
use Phalcon\Logger;
use Phalcon\Logger\Adapter\AdapterInterface;
use Phalcon\Db\Adapter\AdapterInterface as DbAdapterInterface;
use Phalcon\Mvc\Model\Transaction\ManagerInterface as TxnManagerInterface;
use Phalcon\Mvc\Model\TransactionInterface;
use Phalcon\Logger\Item;

class DbAdapter extends BaseAdapter
{
    protected const LOGGER_NAME = 'DatabaseLogger';

    public const CONFIG_TABLE_NAME = 'table_name';
    public const CONFIG_COLUMN_ERROR_LEVEL = 'column_error_level';
    public const CONFIG_COLUMN_MESSAGE = 'column_message';
    public const CONFIG_COLUMN_NAME = 'column_name';
    public const CONFIG_COLUMN_TIMESTAMP = 'column_timeestamp';
    public const CONFIG_DATE_FORMAT = 'date_format';
    public const CONFIG_MIN_LOG_LEVEL = 'min_log_level';

    protected const DEFAULT_TABLE_NAME = 'log';
    protected const DEFAULT_ERROR_LEVEL_COLUMN = 'type';
    protected const DEFAULT_MESSAGE_COLUMN = 'message';
    protected const DEFAULT_TIMESTAMP_COLUMN = 'date_entered';
    protected const DEFAULT_NAME_COLUMN = 'method';
    protected const DEFAULT_DATE_FORMAT = 'Y-m-d H:i:s';
    protected const DEFAULT_MIN_LOG_LEVEL = Logger::DEBUG;

    /**
     * @var DbAdapterInterface
     */
    private $dbAdapter;

    /**
     * @var TransactionInterface
     */
    private $transaction;

    public function getName(): string
    {
        return self::LOGGER_NAME;
    }

    public function begin(): AdapterInterface
    {
        if ($this->transaction === null) {
            $this->transaction = $this->getTransactionManager()
                ->get();
            $this->transaction
                ->begin();
        }

        return parent::begin();
    }

    /**
     * @return AdapterInterface
     */
    public function commit(): AdapterInterface
    {
        try {
            parent::commit();
            if ($this->transaction !== null) {
                $this->transaction
                    ->commit();
                unset($this->transaction);
            }
        }
        catch (\Throwable $e) {
            $this->logLoggerFailure(__METHOD__ . '() failed to commit the trainsaction due to: ' . $e->getMessage() . PHP_EOL . 'Trace: ' . $e->getTraceAsString());
        }

        return $this;
    }

    public function process(Item $item): void
    {
        if ($this->shouldLog($item) === false) {
            return;
        }

        $database = $this->getDatabase();

        $time = $item->getTime();
        try {
            if (!$time instanceof \DateTimeImmutable) {
                if (!empty($time)) {
                    $time = (new \DateTimeImmutable())->setTimestamp($time);
                }

                if (empty($time) || $time === false) {
                    $time = new \DateTimeImmutable('now', new \DateTimeZone('utc'));
                }
            }
        }
        // @codeCoverageIgnoreStart
        catch (\Throwable $e) {
            // Do nothing
        }
        // @codeCoverageIgnoreEnd

        $bind = [
            'errorLevel' => $this->logLevelToString($item->getType()),
            'msg' => $this->getFormatter()->format($item),
            'errorName' => $item->getName(),
            'errorTimestamp' => $time->format($this->options[self::CONFIG_DATE_FORMAT])
        ];
        $bindTypes = [
            'errorLevel' => Column::BIND_PARAM_STR,
            'msg' => Column::BIND_PARAM_STR,
            'errorName' => Column::BIND_PARAM_STR,
            'errorTimestamp' => Column::BIND_PARAM_STR
        ];

        $query = 'INSERT INTO ' . $this->options[self::CONFIG_TABLE_NAME] . ' (' . $this->options[self::CONFIG_COLUMN_ERROR_LEVEL]. ', ' .
            $this->options[self::CONFIG_COLUMN_MESSAGE] . ', ' . $this->options[self::CONFIG_COLUMN_NAME] . ', ' . $this->options[self::CONFIG_COLUMN_TIMESTAMP] . ') VALUES (:errorLevel, :msg, :errorName, :errorTimestamp)';

        $database->query($query, $bind, $bindTypes);
    }

    public function rollback(): AdapterInterface
    {
        try {
            if ($this->transaction !== null) {
                $this->transaction
                    ->rollback();
            }
        }
        catch (\Throwable $e) {
            // Do nothing
        }

        return parent::rollback();
    }

    /**
     * @return DbAdapterInterface
     */
    private function getDatabase(): DbAdapterInterface
    {
        if ($this->dbAdapter === null) {
            if ($this->di->has('db') === false) {
                // @codeCoverageIgnoreStart
                $this->logLoggerFailure('MVS Logger cannot log to database due to the \'db\' service not being set in DI.');
                // @codeCoverageIgnoreEnd
            }

            $this->dbAdapter = $this->di->get('db');
        }

        return $this->dbAdapter;
    }

    private function getTransactionManager(): TxnManagerInterface
    {
        return $this->di
            ->get('transactionManager');
    }

    protected function setOptions(array $options=[]): void
    {
        $optionsNames = [
            self::CONFIG_TABLE_NAME => self::DEFAULT_TABLE_NAME,
            self::CONFIG_COLUMN_ERROR_LEVEL => self::DEFAULT_ERROR_LEVEL_COLUMN,
            self::CONFIG_COLUMN_MESSAGE => self::DEFAULT_MESSAGE_COLUMN,
            self::CONFIG_COLUMN_NAME => self::DEFAULT_NAME_COLUMN,
            self::CONFIG_COLUMN_TIMESTAMP => self::DEFAULT_TIMESTAMP_COLUMN,
            self::CONFIG_DATE_FORMAT => self::DEFAULT_DATE_FORMAT,
        ];

        foreach($optionsNames as $name=>$default_value) {
            if (!isset($options[$name])) {
                $options[$name] = $default_value;
            }
            $this->options[$name] = $options[$name];
        }

        parent::setOptions($options);
    }
}