<?php
namespace Mvs\Logger\Phalcon\Adapter;

use Mvs\Logger\Phalcon\Adapter\Email\MailerException;
use Mvs\Logger\Phalcon\Adapter\Email\MailerInterface;
use Mvs\Logger\Phalcon\Adapter\Email\SendmailMailer;
use Phalcon\Logger\Adapter\AdapterInterface;
use Phalcon\Logger\Item;

class EmailAdapter extends BaseAdapter
{
    protected const LOGGER_NAME = 'EmailLogger';

    public const CONFIG_SUBJECT = 'subject';
    public const CONFIG_ENVIRONMENT = 'environment';

    protected const DEFAULT_SUBJECT = '{errorLevel} Report';

    /**
     * @var bool
     */
    private $is_transaction = false;

    /**
     * @var array
     */
    private $queued_messages = [];

    /**
     * @var MailerInterface
     */
    private $mailer;

    public function __construct(array $options = [], MailerInterface $mailer=null)
    {
        if ($mailer === null) {
            $mailer = new SendmailMailer();
        }
        $this->mailer = $mailer;

        parent::__construct($options);
    }

    public function getName(): string
    {
        return self::LOGGER_NAME;
    }

    public function begin(): AdapterInterface
    {
        $this->is_transaction = true;
        return parent::begin();
    }

    public function add(Item $item): AdapterInterface
    {
        if ($this->shouldLog($item)) {
            parent::add($item);
        }

        return $this;
    }

    public function commit(): AdapterInterface
    {
        $this->is_transaction = false;
        return parent::commit();
    }

    public function process(Item $item): void
    {
        if ($this->shouldLog($item) === false) {
            return;
        }

        $this->queued_messages[] = $this->getFormatter()->format($item);

        if ($this->is_transaction === false) {
            $environment = $this->getOption(self::CONFIG_ENVIRONMENT);
            $context = [
                'errorLevel' => ucfirst($this->logLevelToString($item->getType())),
                'environment' => ($environment ?? (' (' . $environment . ')')),
            ];

            $subject = $this->formatSubject($this->getOption(self::CONFIG_SUBJECT), $context);
            $message = implode(PHP_EOL . PHP_EOL, $this->queued_messages);
            $this->mailer
                ->send($subject, $message);
            $this->queued_messages = [];
        }
    }

    public function rollback(): AdapterInterface
    {
        $this->is_transaction = false;
        $this->queued_messages = [];
        return parent::rollback();
    }

    public function getOption(string $name)
    {
        $optionValue = parent::getOption($name);
        if ($optionValue === null) {
            $optionValue = $this->mailer
                ->getOption($name);
        }

        return $optionValue;
    }

    /**
     * @param array $options
     * @throws MailerException
     */
    protected function setOptions(array $options=[]): void
    {
        if (isset($options[self::CONFIG_SUBJECT])) {
            $this->options[self::CONFIG_SUBJECT] = $options[self::CONFIG_SUBJECT];
        }
        else {
            $this->options[self::CONFIG_SUBJECT] = self::DEFAULT_SUBJECT;
        }

        if (isset($options[self::CONFIG_ENVIRONMENT])) {
            $this->options[self::CONFIG_ENVIRONMENT] = $options[self::CONFIG_ENVIRONMENT];
        }

        $this->mailer
            ->setOptions($options);

        parent::setOptions($options);
    }

    private function formatSubject(string $subject, array $context): string
    {
        $replace = [];
        if (!empty($context)) {
            foreach($context as $key=>$value) {
                $replace['{' . $key . '}'] = $value;
            }

            $subject = \strtr($subject, $replace);
        }

        return $subject;
    }
}