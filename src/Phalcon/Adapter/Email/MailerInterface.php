<?php
namespace Mvs\Logger\Phalcon\Adapter\Email;

interface MailerInterface
{
    public function setOptions(array $options): void;

    /**
     * @param $subject
     * @param $message
     * @return bool
     * @throws MailerException
     */
    public function send($subject, $message): bool;

    /**
     * @param string $name
     * @return mixed|null
     */
    public function getOption(string $name);
}