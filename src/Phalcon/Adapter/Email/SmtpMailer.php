<?php
namespace Mvs\Logger\Phalcon\Adapter\Email;

class SmtpMailer extends BaseMailer
{
    public const CONFIG_SMTP_USER = 'smtp_user';
    public const CONFIG_SMTP_PASS = 'smtp_pass';
    public const CONFIG_SMTP_HOST = 'smtp_host';
    public const CONFIG_SMTP_PORT = 'smtp_port';
    public const CONFIG_SMTP_ENCRYPTION = 'smtp_receiption';

    protected const DEFAULT_SMTP_PORT = 587;
    protected const DEFAULT_SMTP_ENCRYPTION = 'tls'; // false, ssl or tls

    public function setOptions(array $options): void
    {
        if (!isset($options[self::CONFIG_SMTP_HOST])) {
            throw new MailerException('Email logger set for SMTP but no host defined in config.', 1);
        }
        if (!isset($options[self::CONFIG_SMTP_USER])) {
            throw new MailerException('Email logger set for SMTP but no user defined in config.', 2);
        }
        if (!isset($options[self::CONFIG_SMTP_PASS])) {
            throw new MailerException('Email logger set for SMTP but no password defined in config.', 3);
        }

        $this->options[self::CONFIG_SMTP_HOST] = $options[self::CONFIG_SMTP_HOST];
        $this->options[self::CONFIG_SMTP_USER] = $options[self::CONFIG_SMTP_USER];
        $this->options[self::CONFIG_SMTP_PASS] = $options[self::CONFIG_SMTP_PASS];

        if (isset($options[self::CONFIG_SMTP_PORT])) {
            $this->options[self::CONFIG_SMTP_PORT] = $options[self::CONFIG_SMTP_PORT];
        }
        else {
            $this->options[self::CONFIG_SMTP_PORT] = self::DEFAULT_SMTP_PORT;
        }

        if (isset($options[self::CONFIG_SMTP_ENCRYPTION])) {
            $this->options[self::CONFIG_SMTP_ENCRYPTION] = $options[self::CONFIG_SMTP_ENCRYPTION];
        }
        else {
            $this->options[self::CONFIG_SMTP_ENCRYPTION] = self::DEFAULT_SMTP_ENCRYPTION;
        }

        parent::setOptions($options);
    }

    /**
     * @param $subject
     * @param $message
     * @return bool
     * @throws MailerException
     */
    public function send($subject, $message): bool
    {
        $transport = (new \Swift_SmtpTransport($this->options[self::CONFIG_SMTP_HOST], $this->options[self::CONFIG_SMTP_PORT], $this->options[self::CONFIG_SMTP_ENCRYPTION]))
            ->setUsername($this->options[self::CONFIG_SMTP_USER])
            ->setPassword($this->options[self::CONFIG_SMTP_PASS]);

        $mailer = new \Swift_Mailer($transport);

        if (isset($this->options[self::CONFIG_SENDER_NAME])) {
            $sender = [
                $this->options[self::CONFIG_SENDER_EMAIL] => $this->options[self::CONFIG_SENDER_NAME]
            ];
        }
        else {
            $sender = [$this->options[self::CONFIG_SENDER_EMAIL]];
        }

        $message = (new \Swift_Message($subject))
            ->setFrom($sender)
            ->setTo($this->options[self::CONFIG_RECIPIENT])
            ->setBody($message);

        if( $mailer->send($message) > 0) {
            return true;
        }

        throw new MailerException('Failed to send email via SMTP mailer.');
    }

}