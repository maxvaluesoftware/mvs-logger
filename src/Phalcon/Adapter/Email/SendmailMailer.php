<?php
namespace Mvs\Logger\Phalcon\Adapter\Email;

class SendmailMailer extends BaseMailer
{
    public const CONFIG_HEADERS = 'headers';

    public function setOptions(array $options): void
    {
        if (isset($options[self::CONFIG_HEADERS])) {
            $this->options[self::CONFIG_HEADERS] = $options[self::CONFIG_HEADERS];
        }
        else {
            $this->options[self::CONFIG_HEADERS] = [];
        }

        parent::setOptions($options);
    }

    /**
     * @param $subject
     * @param $message
     * @return bool
     * @throws MailerException
     */
    public function send($subject, $message): bool
    {
        $headers = [];
        $from_email = $this->getOption(self::CONFIG_SENDER_EMAIL);
        if ($from_email !== null) {
            $from_name = $this->getOption(self::CONFIG_SENDER_NAME);
            $headers['From'] = ($from_name !== null ? $from_name . ' ' : '') . '<' . $from_email . '>';
        }
        if (!empty($this->options[self::CONFIG_HEADERS])) {
            foreach($this->options[self::CONFIG_HEADERS] as $key=>$val) {
                $headers[$key] = $val;
            }
        }

        if (\is_array($this->options[self::CONFIG_RECIPIENT])) {
            $recipients = implode(',', $this->options[self::CONFIG_RECIPIENT]);
        }
        else {
            $recipients = $this->options[self::CONFIG_RECIPIENT];
        }

        if ($this->mail($recipients, $subject, $message, $headers) === false) {
            throw new MailerException('SendmailMailer failed to send log via email.');
        }

        return true;
    }

    /**
     * @param $recipients
     * @param string $subject
     * @param string $message
     * @param array $headers
     * @return bool
     */
    private function mail($recipients, string $subject, string $message, array $headers): bool
    {
        return mail($recipients, $subject, $message, $headers);
    }
}