<?php
namespace Mvs\Logger\Phalcon\Adapter\Email;

abstract class BaseMailer implements MailerInterface
{
    public const CONFIG_RECIPIENT = 'recipient';
    public const CONFIG_SENDER_EMAIL = 'sender_email';
    public const CONFIG_SENDER_NAME = 'sender_name';

    protected const DEFAULT_SENDER_EMAIL = 'root@localhost';

    protected $options = [];

    /**
     * @param array $options
     * @throws MailerException
     */
    public function setOptions(array $options): void
    {
        if (isset($options[self::CONFIG_RECIPIENT])) {
            $this->options[self::CONFIG_RECIPIENT] = $options[self::CONFIG_RECIPIENT];
        }
        else {
            throw new MailerException('The email logger must have the recipient(s) set in the config.', 0);
        }

        if (isset($options[self::CONFIG_SENDER_EMAIL])) {
            $this->options[self::CONFIG_SENDER_EMAIL] = $options[self::CONFIG_SENDER_EMAIL];
        }
        else {
            $this->options[self::CONFIG_SENDER_EMAIL] = self::DEFAULT_SENDER_EMAIL;
        }

        if (isset($options[self::CONFIG_SENDER_NAME])) {
            $this->options[self::CONFIG_SENDER_NAME] = $options[self::CONFIG_SENDER_NAME];
        }
    }

    /**
     * @param string $name
     * @return mixed|null
     */
    public function getOption(string $name)
    {
        return $this->options[$name] ?? null;
    }
}