<?php /** @noinspection PhpUnused */
namespace Mvs\Logger\Phalcon;

use Phalcon\Di;
use Phalcon\Logger as PhalconLogger;

class Logger
{
    /**
     * @var PhalconLogger
     */
    protected $logger;

    protected function __construct(string $message, array $context, int $logLevel)
    {
        $di = Di::getDefault();
        if ($di === null) {
            // @codeCoverageIgnoreStart
            throw new \RuntimeException('Di must be initialized before logging.', 1);
            // @codeCoverageIgnoreEnd
        }
        if ($di->has('logger') === false) {
            throw new \RuntimeException('The logger service must be set before logging.', 2);
        }

        $this->logger = $di->get('logger');
        $this->logMessage($message, $context, $logLevel);
    }

    public static function emergency(string $message, array $context=[]): void
    {
        new self($message, $context, PhalconLogger::EMERGENCY);
    }

    public static function critical(string $message, array $context=[]): void
    {
        new self($message, $context, PhalconLogger::CRITICAL);
    }

    public static function alert(string $message, array $context=[]): void
    {
        new self($message, $context, PhalconLogger::ALERT);
    }

    public static function error(string $message, array $context=[]): void
    {
        new self($message, $context, PhalconLogger::ERROR);
    }

    public static function warning(string $message, array $context=[]): void
    {
        new self($message, $context, PhalconLogger::WARNING);
    }

    public static function notice(string $message, array $context=[]): void
    {
        new self($message, $context, PhalconLogger::NOTICE);
    }

    public static function info(string $message, array $context=[]): void
    {
        new self($message, $context, PhalconLogger::INFO);
    }

    public static function debug(string $message, array $context=[]): void
    {
        new self($message, $context, PhalconLogger::DEBUG);
    }

    public static function fromException(string $method, \Throwable $e, array $variables=[], int $logLevel=PhalconLogger::CRITICAL): void
    {
        $context = $variables;
        $log_msg = $method . '() caught an exception with message: {message}' . PHP_EOL;
        if (!empty($variables)) {
            foreach($variables as $name=>$value) {
                $log_msg.= $name . ': {' . $name . '}' . PHP_EOL;
            }
        }
        $log_msg.= 'Trace: {trace}' . PHP_EOL;
        $context['message'] = $e->getMessage();
        $context['trace'] = $e->getTraceAsString();

        new self($log_msg, $context, $logLevel);
    }

    protected function logMessage(string $message, array $context, int $logLevel): void
    {
        switch ($logLevel) {
            case PhalconLogger::EMERGENCY:
                $this->logger
                    ->emergency($message, $context);
                break;

            case PhalconLogger::CRITICAL:
                $this->logger
                    ->critical($message, $context);
                break;

            case PhalconLogger::ALERT:
                $this->logger
                    ->alert($message, $context);
                break;

            case PhalconLogger::ERROR:
                $this->logger
                    ->error($message, $context);
                break;

            case PhalconLogger::WARNING:
                $this->logger
                    ->warning($message, $context);
                break;

            case PhalconLogger::NOTICE:
                $this->logger
                    ->notice($message, $context);
                break;

            case PhalconLogger::INFO:
                $this->logger
                    ->info($message, $context);
                break;

            case PhalconLogger::DEBUG:
            default:
                $this->logger
                    ->debug($message, $context);
                break;
        }
    }
}