<?php
namespace Mvs\Logger\Phalcon\Formatter;

use Phalcon\Logger\Formatter\AbstractFormatter;
use Phalcon\Logger\Item;

class DbFormatter extends AbstractFormatter
{
    public function format(Item $item)
    {
        $replace = [];
        $context = $item->getContext();
        $message = $item->getMessage();
        if (\is_array($context)) {
            foreach($context as $key=>$value) {
                $replace['{' . $key . '}'] = $value;
            }

            $message = \strtr($message, $replace);
        }

        return $message;
    }

}