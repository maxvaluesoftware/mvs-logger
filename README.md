# **Mvs/Logger**
Phalcon 4.* Logger extension and adapters.  PSR-3 compliant.

**Requirements**  
Requires PHP 7.2+ and Phalcon 4.0+

**Features**
- Adds EmailAdapter for Phalcon Logger to send erros via email using
  sendmail or SMTP
- Adds DbAdapter for Phalcon Logger to log errors to database
- Both the DbAdapter and the EmailAdapter accept a CONFIG_MIN_LOG_LEVEL config to specify the minimum log level to log to the adapter.
- Add static method logging instead of requiring the Phalcon Logger to
  be injected

**Installation**
`composer require maxvaluesoftware\logger "^4.0`

**Usage**

***EmailAdapter***  
The email adapter can accept two mailers; either the default sendmail or
SMTP. Each has their own configurations. The mailer configs are public
constants named CONFIG_

*SendmailMailer*  
*Configs*:

    SendmailMailer::CONFIG_RECIPIENT => 'recipient@gmail.com',
    SendmailMailer::CONFIG_SENDER_EMAIL => 'sender@gmail.com',
    SendmailMailer::CONFIG_SENDER_NAME => 'Sender Name',
    SendmailMailer::CONFIG_HEADERS => ['X-Mailer' => PHP_VERSION], // Array of optional headers

Example:
`$options = [];
$adapters[] = new EmailAdapter($configs);
$logger = (new Logger\LoggerFactory(new
Logger\AdapterFactory()))->newInstance('logger', $adapters);`


*SmtpMailer*  
*Configs*:

    SmtpMailer::CONFIG_RECIPIENT => ['recipient1@gmail.com', 'recipient2@gmail.com'],
    SmtpMailer::CONFIG_SMTP_HOST => 'smtp.gmail.com',
    SmtpMailer::CONFIG_SMTP_PORT => 587,
    SmtpMailer::CONFIG_SMTP_USER => 'smtp username/email',
    SmtpMailer::CONFIG_SMTP_PASS => 'smtp password,
    SmtpMailer::CONFIG_SENDER_EMAIL => 'sender@gmail.com',
    SmtpMailer::CONFIG_SENDER_NAME => 'Sender Name',
    SmtpMailer::CONFIG_SMTP_ENCRYPTION => 'tls', // tls, ssl or null  


`$options = [];
$adapters[] = new EmailAdapter($configs);
$logger = (new Logger\LoggerFactory(new
Logger\AdapterFactory()))->newInstance('logger', $adapters);`

***DbAdapter***  
The database adapter allows logging to a configured Phalcon database
table.

Configs:

    DbAdapter::CONFIG_TABLE_NAME => 'logs',
    DbAdapter::CONFIG_COLUMN_ERROR_LEVEL => 'error_level',
    DbAdapter::CONFIG_COLUMN_MESSAGE => 'error_msg',
    DbAdapter::CONFIG_COLUMN_NAME => 'name',
    DbAdapter::CONFIG_COLUMN_TIMESTAMP => 'timesetamp',
    DbAdapter::CONFIG_DATE_FORMAT => 'Y-m-d H:i:s',
    DbAdapter::CONFIG_MIN_LOG_LEVEL => Logger::ERROR,

Example:
`$adapters[] = new DbAdapter($config);
$logger = (new Logger\LoggerFactory(new Logger\AdapterFactory()))
            ->newInstance('test-logger', $adapters)`

***Static Method Logging***  
Examples:  
`\Mvs\Logger\Phalcon\Logger::critical('Critical message with {variable}', ['variable' => 'inserted value']);`

`\Mvs\Logger\Phalcon\Logger::debug('Debug log message with no variables');`


**Unit Testing**  
Requires editing the tests/config.php file prior to running.