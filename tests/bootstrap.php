<?php
use Phalcon\Config;
use Phalcon\Di\FactoryDefault;
use Phalcon\Di;
use Phalcon\Db\Adapter\Pdo\Mysql;

require_once __DIR__ . '/../vendor/autoload.php';
/**
 * @var Config $config
 */
$config = include 'config.php';
/**
 * Register services
 */
$di = new FactoryDefault();
Di::reset();

Di::setDefault($di);
$di->setShared('config', $config);
$di->setShared(
    'pdo',
    function () use ($config) {
        /**
         * @var Config $dbConfig
         */
        $dbConfig = $config->get('database');
        return new \PDO('mysql:host=' . $dbConfig->get('host'), $dbConfig->get('user'), $dbConfig->get('pass'), [\PDO::ATTR_PERSISTENT => 1]);
    }
);

$di->setShared(
    'db',
    function () use ($config) {
        /**
         * @var Config $dbConfig
         */
        $dbConfig = $config->get('database');
        return new Mysql([
            'host' => $dbConfig->get('host'),
            'port' => $dbConfig->get('port'),
            'username' => $dbConfig->get('user'),
            'password' => $dbConfig->get('pass'),
            'dbname' => $dbConfig->get('dbname'),
            'charset' => $dbConfig->get('charset'),
            'options' => [
                \PDO::ATTR_PERSISTENT => 1,
                \PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
            ],
        ]);
    }
);