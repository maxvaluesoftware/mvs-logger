<?php
use Phalcon\Config;

$config = [];
$config['smtpMailer'] = [
    'host' => '',
    'port' => 587,
    'user' => '',
    'pass' => '',
    'encryption' => 'tls', // null, ssl or tls
    'recipient_1' => '',
    'recipient_2' => ''
];

$config['sendmailMailer'] = [
    'sender_email' => '',
    'sender_name' => '',
    'recipient_1' => '',
    'recipient_2' => ''
];

$config['database'] = [
    'adapter' => 'Mysql',
    'host' => 'mysql',
    'user' => 'phalcon',
    'pass' => 'secret',
    'dbname' => 'logger',
    'charset' => 'utf8',
    'port' => 3306,
    'persistent' => true,
];

return new Config($config);