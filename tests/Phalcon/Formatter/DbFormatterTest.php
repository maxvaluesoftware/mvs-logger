<?php
namespace Mvs\Logger\Phalcon\Formatter;

use Phalcon\Logger;
use Phalcon\Logger\Item;
use PHPUnit\Framework\TestCase;

class DbFormatterTest extends TestCase
{
    public function testFormat(): void
    {
        $string = 'Test string {var}';
        $var = 'monkey';
        $expected_string = str_replace('{var}', $var, $string);

        $dbFormatter = new DbFormatter();

        $item = new Item($string, 'test', Logger::CRITICAL, 0, ['var' => $var]);

        self::assertEquals($expected_string, $dbFormatter->format($item));
    }
}