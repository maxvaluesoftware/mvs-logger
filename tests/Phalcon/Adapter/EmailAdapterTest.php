<?php
namespace Mvs\Logger\Adapter;

use Mvs\Logger\Phalcon\Adapter\Email\BaseMailer;
use Mvs\Logger\Phalcon\Adapter\Email\MailerInterface;
use Mvs\Logger\Phalcon\Adapter\EmailAdapter;
use Phalcon\Di;
use Phalcon\Logger;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class EmailAdapterTest extends TestCase
{
    /**
     * @var MailerInterface|MockObject
     */
    private $mailer;

    public function setUp(): void
    {
        parent::setUp();

        $this->mailer = $this->getMockBuilder(MailerInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
    }

    public function testConstructor(): void
    {
        $configs = [
            BaseMailer::CONFIG_RECIPIENT => 'john.doe@gmail.com',
            EmailAdapter::CONFIG_ENVIRONMENT => 'testing',
            EmailAdapter::CONFIG_SUBJECT => 'Test subject',
            EmailAdapter::CONFIG_MIN_LOG_LEVEL => Logger::CRITICAL,
        ];

        $emailAdapter = new EmailAdapter($configs, $this->mailer);
        self::assertEquals($configs[EmailAdapter::CONFIG_ENVIRONMENT], $emailAdapter->getOption(EmailAdapter::CONFIG_ENVIRONMENT));
        self::assertEquals($configs[EmailAdapter::CONFIG_SUBJECT], $emailAdapter->getOption(EmailAdapter::CONFIG_SUBJECT));
        self::assertEquals($configs[EmailAdapter::CONFIG_MIN_LOG_LEVEL], $emailAdapter->getOption(EmailAdapter::CONFIG_MIN_LOG_LEVEL));
    }

    public function testConstructorWithDefaults(): void
    {
        $configs = [
            BaseMailer::CONFIG_RECIPIENT => 'john.doe@gmail.com',
        ];
        $emailAdapter = new EmailAdapter($configs);
        self::assertNull($emailAdapter->getOption(EmailAdapter::CONFIG_ENVIRONMENT));
        self::assertEquals('{errorLevel} Report', $emailAdapter->getOption(EmailAdapter::CONFIG_SUBJECT));
        self::assertEquals(Logger::DEBUG, $emailAdapter->getOption(EmailAdapter::CONFIG_MIN_LOG_LEVEL));
    }

    public function testGetName(): void
    {
        $configs = [
            BaseMailer::CONFIG_RECIPIENT => 'john.doe@gmail.com',
        ];
        $emailAdapter = new EmailAdapter($configs);
        self::assertEquals('EmailLogger', $emailAdapter->getName());
    }

    public function testLogNoTransaction(): void
    {
        $message = 'Test critical message';
        $configs = [
            BaseMailer::CONFIG_RECIPIENT => 'john.doe@gmail.com',
            EmailAdapter::CONFIG_SUBJECT => 'Test subject',
        ];
        /** @noinspection PhpParamsInspection */
        $this->mailer
            ->expects(self::once())
            ->method('send')
            ->with(self::equalTo($configs[EmailAdapter::CONFIG_SUBJECT]), self::stringContains($message));

        $adapter = $this->getAdapter($configs, $this->mailer);

        $logger = $this->getLogger($adapter);

        $logger->critical($message);
    }

    public function testLogWithTransactionSuccess(): void
    {
        $message = 'Test critical message';
        $configs = [
            BaseMailer::CONFIG_RECIPIENT => 'john.doe@gmail.com',
            EmailAdapter::CONFIG_SUBJECT => 'Test subject',
        ];
        /** @noinspection PhpParamsInspection */
        $this->mailer
            ->expects(self::once())
            ->method('send')
            ->with(self::equalTo($configs[EmailAdapter::CONFIG_SUBJECT]), self::stringContains($message));
        $adapter = $this->getAdapter($configs, $this->mailer);
        $adapter->begin();

        $logger = $this->getLogger($adapter);

        $logger->critical($message);
        $adapter->commit();
    }

    public function testLogTransactionRollback(): void
    {
        $message = 'Test critical message';
        $configs = [
            BaseMailer::CONFIG_RECIPIENT => 'john.doe@gmail.com',
            EmailAdapter::CONFIG_SUBJECT => 'Test subject',
        ];
        $this->mailer
            ->expects(self::never())
            ->method('send');
        $adapter = $this->getAdapter($configs, $this->mailer);
        $adapter->begin();

        $logger = $this->getLogger($adapter);

        $logger->critical($message);
        $adapter->rollback();
    }

    protected function getAdapter(array $config=[], MailerInterface $mailer=null): EmailAdapter
    {
        $adapter = new EmailAdapter($config, $mailer);
        $di = Di::getDefault();
        $adapter->setDI($di);

        return $adapter;
    }

    protected function getLogger(EmailAdapter $adapter): Logger
    {
        $adapters = [
            $adapter->getName() => $adapter
        ];

        return (new Logger\LoggerFactory(new Logger\AdapterFactory()))
            ->newInstance('logger', $adapters);
    }
}