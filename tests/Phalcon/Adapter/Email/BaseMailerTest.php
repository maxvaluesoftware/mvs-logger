<?php
namespace Mvs\Logger\Phalcon\Adapter\Email;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class BaseMailerTest extends TestCase
{
    /**
     * @var BaseMailer|MockObject
     */
    protected $mailer;

    private $default_sender = 'root@localhost';

    public function setUp(): void
    {
        parent::setUp();

        $this->mailer = $this->getMockForAbstractClass(BaseMailer::class);
    }

    public function testSetOptions(): void
    {
        $options = [
            BaseMailer::CONFIG_RECIPIENT => 'recipient@phpunit.com',
            BaseMailer::CONFIG_SENDER_EMAIL => 'sender@phpunit.com',
            BaseMailer::CONFIG_SENDER_NAME => 'Unit Test',
        ];

        $this->mailer
            ->setOptions($options);

        self::assertEquals($options[BaseMailer::CONFIG_RECIPIENT], $this->mailer->getOption(BaseMailer::CONFIG_RECIPIENT));
        self::assertEquals($options[BaseMailer::CONFIG_SENDER_EMAIL], $this->mailer->getOption(BaseMailer::CONFIG_SENDER_EMAIL));
        self::assertEquals($options[BaseMailer::CONFIG_SENDER_NAME], $this->mailer->getOption(BaseMailer::CONFIG_SENDER_NAME));
    }

    public function testSetOptionsMissingRecipient(): void
    {
        $this->expectException(MailerException::class);
        $this->expectExceptionCode(0);

        $this->mailer
            ->setOptions([]);
    }

    public function testOptionsWithDefaults(): void
    {
        $options = [
            BaseMailer::CONFIG_RECIPIENT => 'recipient@phpunit.com',
        ];

        $this->mailer
            ->setOptions($options);

        self::assertEquals($options[BaseMailer::CONFIG_RECIPIENT], $this->mailer->getOption(BaseMailer::CONFIG_RECIPIENT));
        self::assertEquals($this->default_sender, $this->mailer->getOption(BaseMailer::CONFIG_SENDER_EMAIL));
    }
}