<?php
namespace Mvs\Logger\Phalcon\Adapter\Email;

use Phalcon\Config;
use Phalcon\Di;
use PHPUnit\Framework\TestCase;

class SmtpMailerTest extends TestCase
{
    /**
     * @var SmtpMailer
     */
    protected $mailer;

    private $subject = 'SmtpMailer Unit Test';

    private $user;
    private $pass;
    private $host;
    private $port = 587;
    private $encryption = 'tls';

    /**
     * @var string
     */
    protected $recipient1;

    /**
     * @var string|null
     */
    protected $recipient2;

    public function setUp(): void
    {
        parent::setUp();

        $this->mailer = new SmtpMailer();

        /**
         * @var Config $config
         */
        $config = Di::getDefault()
            ->get('config');
        /**
         * @var Config $sendMailConfig
         */
        $smtpMailerConfig = $config
            ->get('smtpMailer');

        $this->user = $smtpMailerConfig->get('user');
        $this->pass = $smtpMailerConfig->get('pass');
        $this->host = $smtpMailerConfig->get('host');
        $this->port = $smtpMailerConfig->get('port');
        $this->encryption = $smtpMailerConfig->get('encryption');
        $this->recipient1 = $smtpMailerConfig->get('recipient_1');
        $this->recipient2 = $smtpMailerConfig->get('recipient_2');
    }

    public function testSetOptions(): void
    {
        $options = [
            SmtpMailer::CONFIG_RECIPIENT => 'recipient@phpunit.com',
            SmtpMailer::CONFIG_SMTP_HOST => $this->host,
            SmtpMailer::CONFIG_SMTP_PORT => $this->port,
            SmtpMailer::CONFIG_SMTP_USER => $this->user,
            SmtpMailer::CONFIG_SMTP_PASS => $this->pass,
            SmtpMailer::CONFIG_SMTP_ENCRYPTION => 'ssl',
        ];

        $this->mailer
            ->setOptions($options);

        self::assertEquals($options[SmtpMailer::CONFIG_RECIPIENT], $this->mailer->getOption(SmtpMailer::CONFIG_RECIPIENT));
        self::assertEquals($options[SmtpMailer::CONFIG_SMTP_HOST], $this->mailer->getOption(SmtpMailer::CONFIG_SMTP_HOST));
        self::assertEquals($options[SmtpMailer::CONFIG_SMTP_PORT], $this->mailer->getOption(SmtpMailer::CONFIG_SMTP_PORT));
        self::assertEquals($options[SmtpMailer::CONFIG_SMTP_USER], $this->mailer->getOption(SmtpMailer::CONFIG_SMTP_USER));
        self::assertEquals($options[SmtpMailer::CONFIG_SMTP_PASS], $this->mailer->getOption(SmtpMailer::CONFIG_SMTP_PASS));
        self::assertEquals($options[SmtpMailer::CONFIG_SMTP_ENCRYPTION], $this->mailer->getOption(SmtpMailer::CONFIG_SMTP_ENCRYPTION));
    }

    public function testSetOptionsWithDefaults(): void
    {
        $options = [
            SmtpMailer::CONFIG_RECIPIENT => 'recipient@phpunit.com',
            SmtpMailer::CONFIG_SMTP_HOST => $this->host,
            SmtpMailer::CONFIG_SMTP_USER => $this->user,
            SmtpMailer::CONFIG_SMTP_PASS => $this->pass,
        ];

        $this->mailer
            ->setOptions($options);

        self::assertEquals($options[SmtpMailer::CONFIG_RECIPIENT], $this->mailer->getOption(SmtpMailer::CONFIG_RECIPIENT));
        self::assertEquals($options[SmtpMailer::CONFIG_SMTP_HOST], $this->mailer->getOption(SmtpMailer::CONFIG_SMTP_HOST));
        self::assertEquals(587, $this->mailer->getOption(SmtpMailer::CONFIG_SMTP_PORT));
        self::assertEquals($options[SmtpMailer::CONFIG_SMTP_USER], $this->mailer->getOption(SmtpMailer::CONFIG_SMTP_USER));
        self::assertEquals($options[SmtpMailer::CONFIG_SMTP_PASS], $this->mailer->getOption(SmtpMailer::CONFIG_SMTP_PASS));
        self::assertEquals('tls', $this->mailer->getOption(SmtpMailer::CONFIG_SMTP_ENCRYPTION));
    }

    public function testSetOptionsMissingHost(): void
    {
        $this->expectException(MailerException::class);
        $this->expectExceptionCode(1);

        $options = [
            SmtpMailer::CONFIG_RECIPIENT => 'recipient@phpunit.com',
            SmtpMailer::CONFIG_SMTP_PORT => $this->port,
            SmtpMailer::CONFIG_SMTP_USER => $this->user,
            SmtpMailer::CONFIG_SMTP_PASS => $this->pass,
        ];

        $this->mailer
            ->setOptions($options);
    }

    public function testSetOptionsMissingUser(): void
    {
        $this->expectException(MailerException::class);
        $this->expectExceptionCode(2);

        $options = [
            SmtpMailer::CONFIG_RECIPIENT => 'recipient@phpunit.com',
            SmtpMailer::CONFIG_SMTP_PORT => $this->port,
            SmtpMailer::CONFIG_SMTP_HOST => $this->host,
            SmtpMailer::CONFIG_SMTP_PASS => $this->pass,
        ];

        $this->mailer
            ->setOptions($options);
    }

    public function testSetOptionsMissingPass(): void
    {
        $this->expectException(MailerException::class);
        $this->expectExceptionCode(3);

        $options = [
            SmtpMailer::CONFIG_RECIPIENT => 'recipient@phpunit.com',
            SmtpMailer::CONFIG_SMTP_PORT => $this->port,
            SmtpMailer::CONFIG_SMTP_HOST => $this->host,
            SmtpMailer::CONFIG_SMTP_USER => $this->user,
        ];

        $this->mailer
            ->setOptions($options);
    }

    public function testSendOneRecipient(): void
    {
        $options = [
            SmtpMailer::CONFIG_RECIPIENT => $this->recipient1,
            SmtpMailer::CONFIG_SMTP_HOST => $this->host,
            SmtpMailer::CONFIG_SMTP_PORT => $this->port,
            SmtpMailer::CONFIG_SMTP_USER => $this->user,
            SmtpMailer::CONFIG_SMTP_PASS => $this->pass,
            SmtpMailer::CONFIG_SENDER_EMAIL => $this->user,
            SmtpMailer::CONFIG_SENDER_NAME => 'PhpUnit SysAdmin Mail',
            SmtpMailer::CONFIG_SMTP_ENCRYPTION => $this->encryption,
        ];

        $this->mailer
            ->setOptions($options);
        $result = $this->mailer
            ->send($this->subject, 'MVS Logger Smtp Unit Test');
        self::assertTrue($result);
    }


    public function testSendMultipleRecipient(): void
    {
        $options = [
            SmtpMailer::CONFIG_RECIPIENT => [$this->recipient1, $this->recipient2],
            SmtpMailer::CONFIG_SMTP_HOST => $this->host,
            SmtpMailer::CONFIG_SMTP_PORT => $this->port,
            SmtpMailer::CONFIG_SMTP_USER => $this->user,
            SmtpMailer::CONFIG_SMTP_PASS => $this->pass,
            SmtpMailer::CONFIG_SENDER_EMAIL => $this->user,
            SmtpMailer::CONFIG_SENDER_NAME => 'PhpUnit SysAdmin Mail',
            SmtpMailer::CONFIG_SMTP_ENCRYPTION => $this->encryption,
        ];

        $this->mailer
            ->setOptions($options);
        $result = $this->mailer
            ->send($this->subject, 'MVS Logger Smtp Unit Test');
        self::assertTrue($result);
    }

    public function testSendWithDefaultSender(): void
    {
        $options = [
            SmtpMailer::CONFIG_RECIPIENT => $this->recipient1,
            SmtpMailer::CONFIG_SMTP_HOST => $this->host,
            SmtpMailer::CONFIG_SMTP_PORT => $this->port,
            SmtpMailer::CONFIG_SMTP_USER => $this->user,
            SmtpMailer::CONFIG_SMTP_PASS => $this->pass,
            SmtpMailer::CONFIG_SMTP_ENCRYPTION => $this->encryption,
        ];

        $this->mailer
            ->setOptions($options);
        $result = $this->mailer
            ->send($this->subject, 'MVS Logger Smtp Unit Test');
        self::assertTrue($result);
    }
}