<?php
namespace Mvs\Logger\Adapter;

use Mvs\Logger\Phalcon\Adapter\DbAdapter;
use Phalcon\Db\Adapter\AdapterInterface;
use Phalcon\Di;
use Phalcon\Logger;
use Phalcon\Mvc\Model\Transaction\Manager;
use Phalcon\Mvc\Model\TransactionInterface;
use PHPUnit\Framework\TestCase;

class DbAdapterTest extends TestCase
{
    /**
     * @var AdapterInterface $db
     */
    protected $db;

    static protected $options = [
        DbAdapter::CONFIG_TABLE_NAME => 'logs',
        DbAdapter::CONFIG_COLUMN_ERROR_LEVEL => 'error_level',
        DbAdapter::CONFIG_COLUMN_MESSAGE => 'error_msg',
        DbAdapter::CONFIG_COLUMN_NAME => 'name',
        DbAdapter::CONFIG_COLUMN_TIMESTAMP => 'timesetamp',
        DbAdapter::CONFIG_DATE_FORMAT => 'Y-m-d H:i:s',
        DbAdapter::CONFIG_MIN_LOG_LEVEL => Logger::ERROR,
    ];

    public function setUp(): void
    {
        parent::setUp();

        $this->db = (Di::getDefault()->get('db'));

        $this->db->query('DELETE FROM ' . self::$options[DbAdapter::CONFIG_TABLE_NAME]);
    }

    public static function setUpBeforeClass(): void
    {
        /**
         * @var AdapterInterface $db
         */
        $db = (Di::getDefault())->get('db');

        $query = "CREATE TABLE IF NOT EXISTS `" . self::$options[DbAdapter::CONFIG_TABLE_NAME] . "` (
    `id_log` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
    `" . self::$options[DbAdapter::CONFIG_COLUMN_ERROR_LEVEL] . "` enum('debug','error','info','notice','warning','alert','critical','emergency') NOT NULL,
    `" . self::$options[DbAdapter::CONFIG_COLUMN_MESSAGE] . "` text NOT NULL,
    `" . self::$options[DbAdapter::CONFIG_COLUMN_NAME] . "` varchar(255) DEFAULT NULL,
    `" . self::$options[DbAdapter::CONFIG_COLUMN_TIMESTAMP] . "` datetime NOT NULL,
    PRIMARY KEY (`id_log`),
    KEY `date_entered` (`" . self::$options[DbAdapter::CONFIG_COLUMN_TIMESTAMP] . "`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;";

        $db->query($query);
    }

    public function testConstructor(): void
    {
        $adapter = new DbAdapter(self::$options);
        self::assertInstanceOf(DbAdapter::class, $adapter);
        self::assertEquals(self::$options[DbAdapter::CONFIG_TABLE_NAME], $adapter->getOption(DbAdapter::CONFIG_TABLE_NAME));
        self::assertEquals(self::$options[DbAdapter::CONFIG_COLUMN_ERROR_LEVEL], $adapter->getOption(DbAdapter::CONFIG_COLUMN_ERROR_LEVEL));
        self::assertEquals(self::$options[DbAdapter::CONFIG_COLUMN_MESSAGE], $adapter->getOption(DbAdapter::CONFIG_COLUMN_MESSAGE));
        self::assertEquals(self::$options[DbAdapter::CONFIG_COLUMN_NAME], $adapter->getOption(DbAdapter::CONFIG_COLUMN_NAME));
        self::assertEquals(self::$options[DbAdapter::CONFIG_COLUMN_TIMESTAMP], $adapter->getOption(DbAdapter::CONFIG_COLUMN_TIMESTAMP));
        self::assertEquals(self::$options[DbAdapter::CONFIG_DATE_FORMAT], $adapter->getOption(DbAdapter::CONFIG_DATE_FORMAT));
        self::assertEquals(self::$options[DbAdapter::CONFIG_MIN_LOG_LEVEL], $adapter->getOption(DbAdapter::CONFIG_MIN_LOG_LEVEL));
    }

    public function testConstructorWithDefaults(): void
    {
        $adapter = new DbAdapter();
        self::assertInstanceOf(DbAdapter::class, $adapter);
        self::assertEquals('log', $adapter->getOption(DbAdapter::CONFIG_TABLE_NAME));
        self::assertEquals('type', $adapter->getOption(DbAdapter::CONFIG_COLUMN_ERROR_LEVEL));
        self::assertEquals('message', $adapter->getOption(DbAdapter::CONFIG_COLUMN_MESSAGE));
        self::assertEquals('method', $adapter->getOption(DbAdapter::CONFIG_COLUMN_NAME));
        self::assertEquals('date_entered', $adapter->getOption(DbAdapter::CONFIG_COLUMN_TIMESTAMP));
        self::assertEquals('Y-m-d H:i:s', $adapter->getOption(DbAdapter::CONFIG_DATE_FORMAT));
        self::assertEquals(Logger::DEBUG, $adapter->getOption(DbAdapter::CONFIG_MIN_LOG_LEVEL));
    }

    public function testLogNoTransaction(): void
    {
        $logger = $this->getLogger($this->getAdapter(self::$options));
        $logger->critical('test log message', [
            'var' => 'testing'
        ]);

        $result = $this->db
            ->query('SELECT * FROM ' . self::$options[DbAdapter::CONFIG_TABLE_NAME]);
        $results = $result->fetchAll();
        self::assertCount(1, $results);
    }

    public function testLogTransactionSuccess(): void
    {
        $adapter = $this->getAdapter(self::$options);
        $adapter->begin();
        $logger = $this->getLogger($adapter);
        $logger->critical('test log message', [
            'var' => 'testing'
        ]);

        $result = $this->db
            ->query('SELECT * FROM ' . self::$options[DbAdapter::CONFIG_TABLE_NAME]);
        $results = $result->fetchAll();
        self::assertCount(0, $results, 'Transaction count should be empty before commit.');

        $adapter->commit();

        $result = $this->db
            ->query('SELECT * FROM ' . self::$options[DbAdapter::CONFIG_TABLE_NAME]);
        $results = $result->fetchAll();
        self::assertCount(1, $results, 'Committed transaction count.');
    }

    public function testLogTransactionRollback(): void
    {
        $adapter = $this->getAdapter(self::$options);
        $adapter->begin();
        $logger = $this->getLogger($adapter);
        $logger->critical('test log message', [
            'var' => 'testing'
        ]);

        $adapter->rollback();
        $result = $this->db
            ->query('SELECT * FROM ' . self::$options[DbAdapter::CONFIG_TABLE_NAME]);
        $results = $result->fetchAll();
        self::assertCount(0, $results, 'Rolled back transaction count.');

        $adapter->commit();
        $result = $this->db
            ->query('SELECT * FROM ' . self::$options[DbAdapter::CONFIG_TABLE_NAME]);
        $results = $result->fetchAll();
        self::assertCount(0, $results, 'Rolled back then committed transaction count.');
    }

    public function testLogTransactionRollbackException(): void
    {
        $mockTransaction = $this->getMockBuilder(TransactionInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
        $mockTransaction
            ->method('rollback')
            ->willThrowException(new \RuntimeException('Failed to rollback.'));

        $mockTransactionManager = $this->getMockBuilder(Manager::class)
            ->disableOriginalConstructor()
            ->getMock();
        $mockTransactionManager
            ->method('get')
            ->willReturn($mockTransaction);

        (Di::getDefault())->setShared('transactionManager', $mockTransactionManager);

        $adapter = $this->getAdapter(self::$options);
        $adapter->begin();
        $logger = $this->getLogger($adapter);
        $logger->critical('test log message', [
            'var' => 'testing'
        ]);

        $adapter->rollback();
        $result = $this->db
            ->query('SELECT * FROM ' . self::$options[DbAdapter::CONFIG_TABLE_NAME]);
        $results = $result->fetchAll();
        self::assertCount(0, $results, 'Rolled back transaction count.');
    }

    /**
     * @param int $min_log_level
     * @param string $level
     * @param bool $should_log
     * @dataProvider minLogLevelProvider
     */
    public function testMinLogLevel(int $min_log_level, string $level, bool $should_log): void
    {
        $adapter = $this->getAdapter([
            DbAdapter::CONFIG_TABLE_NAME => 'logs',
            DbAdapter::CONFIG_COLUMN_ERROR_LEVEL => 'error_level',
            DbAdapter::CONFIG_COLUMN_MESSAGE => 'error_msg',
            DbAdapter::CONFIG_COLUMN_NAME => 'name',
            DbAdapter::CONFIG_COLUMN_TIMESTAMP => 'timesetamp',
            DbAdapter::CONFIG_DATE_FORMAT => 'Y-m-d H:i:s',
            DbAdapter::CONFIG_MIN_LOG_LEVEL => $min_log_level
        ]);
        $logger = $this->getLogger($adapter);

        switch ($level) {
            case 'warning':
                $logger->warning('Test warning message.');
                break;

            case 'critical':
                $logger->critical('Test critical message.');
                break;

            case 'emergency':
                $logger->emergency('Test emergency message.');
                break;

            default:
                // @todo

        }

        $count = ($should_log ? 1 : 0);
        $result = $this->db
            ->query('SELECT * FROM ' . self::$options[DbAdapter::CONFIG_TABLE_NAME]);
        $results = $result->fetchAll();
        self::assertCount($count, $results);
    }

    public function minLogLevelProvider(): array
    {
        return [
            'min: critical, logged: warning' => [
                Logger::CRITICAL, 'warning', false
            ],
            'min: critical, logged: critical' => [
                Logger::CRITICAL, 'critical', true
            ],
            'min: critical, logged: emergency' => [
                Logger::CRITICAL, 'emergency', true
            ]
        ];
    }

    protected function getAdapter(array $config): DbAdapter
    {
        $adapter = new DbAdapter($config);
        $di = Di::getDefault();
        $mockLogger = $this->getMockBuilder(Logger::class)
            ->disableOriginalConstructor()
            ->getMock();
        $di->setShared('logger', $mockLogger);
        $adapter->setDI($di);

        return $adapter;
    }

    protected function getLogger(DbAdapter $adapter): Logger
    {
        $adapters = [
            $adapter->getName() => $adapter
        ];

        return (new Logger\LoggerFactory(new Logger\AdapterFactory()))
            ->newInstance('test-logger', $adapters);
    }
}