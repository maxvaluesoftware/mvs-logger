<?php
namespace Mvs\Logger\Adapter;

use Mvs\Logger\Phalcon\Adapter\BaseAdapter;
use Phalcon\Di;
use Phalcon\Logger;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class BaseAdapterTest extends TestCase
{
    /**
     * @var BaseAdapter|MockObject
     */
    protected $adapter;

    public function setUp(): void
    {
        parent::setUp();

        $this->adapter = $this->getMockForAbstractClass(BaseAdapter::class);
    }

    /**
     * @param int $level
     * @param string $expected
     * @dataProvider logLevelToStringProvider
     */
    public function testLogLevelToString(int $level, string $expected): void
    {
        self::assertEquals($expected, $this->adapter
            ->logLevelToString($level));
    }

    public function logLevelToStringProvider(): array
    {
        return [
            'emergency' => [Logger::EMERGENCY, 'emergency'],
            'critical' => [Logger::CRITICAL, 'critical'],
            'alert' => [Logger::ALERT, 'alert'],
            'error' => [Logger::ERROR, 'error'],
            'warning' => [Logger::WARNING, 'warning'],
            'notice' => [Logger::NOTICE, 'notice'],
            'info' => [Logger::INFO, 'info'],
            'debug' => [Logger::DEBUG, 'debug'],
        ];
    }

    public function testSetDi(): void
    {
        $di = $this->getMockBuilder(Di::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->adapter
            ->setDI($di);

        self::assertInstanceOf(Di::class, $this->adapter->getDI());
    }
}