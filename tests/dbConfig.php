<?php
return [
    'adapter' => 'Mysql',
    'host' => 'mysql',
    'user' => 'phalcon',
    'pass' => 'secret',
    'dbname' => 'logger',
    'charset' => 'utf8',
    'port' => 3306,
    'persistent' => true,
];